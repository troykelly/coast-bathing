<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/phone-icon.png">
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png">

	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<script src="https://use.typekit.net/zgc0byv.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
	<link href='https://fonts.googleapis.com/css?family=Lora:400italic,400' rel='stylesheet' type='text/css'>
	
	<link rel="icon shortcut" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/Favicon32.png">
	<link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/Favicon180.png"/>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div class="body-wrap">
	

	<header id="masthead" class="site-header panel" role="banner">
		
		<div class="row">
	
			<div class="columns-4 site-branding">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					<img src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="site logo" />
				</a>
			</div>
							
			<nav id="site-navigation" class="columns-8 navigation-main" role="navigation">

				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'items_wrap' => '<ul id="main-nav" class="">%3$s</ul>' ) ); ?>


				<a href="#" class="fs-mobile-trigger burger-trigger">
					<span></span>
					<span></span>
					<span></span>
				</a>

			
			
			</nav><!-- #site-navigation -->


			<nav class="columns-12 mobile-nav-container">

				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'items_wrap' => '<ul id="mobile-nav" class="">%3$s</ul>', 'container_class' => 'menu-mobile-menu-container', ) ); ?>

			</nav>
		
		</div>

	</header><!-- #masthead -->

	<section id="main" class="site-main">
