<?php get_header(); ?>
	<div class="row">
		<div class="columns-3 right-1">
			<h1 class="blog-title">Blog</h1>
	
			<p class="product-intro"><?php the_field('blog_intro',53); ?></p>
	
			<?php $cats = get_terms('category'); ?>
			<div class="categories">
				<ul class="cats">
					<?php foreach ($cats as $cat): ?>
					 <?php $current = get_queried_object_id() == $cat->term_id? 'current' : ''; ?>
						<li class="<?php echo $current; ?>"><a href="<?php echo get_term_link($cat); ?>"><?php echo $cat->name; ?></a></li>
					<?php endforeach; ?>
					<li class="<?php echo is_home()? '' : ''; ?>"><a href="<?php the_field('blog_link',53); ?>">Show All</a></li>
				</ul>
			</div>
		</div>
		<div class="columns-7 right-1">
			<div class="blog-articles">
				<?php while( have_posts() ): the_post(); ?>
						<article>
							<p class="post-meta"><?php echo forge_saas_posted_on();  ?></p>
							<div class="image-wrapper">
								<a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail('large'); ?>
								</a>
							</div>
							<div class="content-wrap">
								<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<p><?php echo excerpt(35); ?></p>
								<a href="<?php the_permalink(); ?>" class="read-more">Read More</a>
							</div>
						</article>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
	<div class="pagination-wrap">
		<div class="row">
			<div class="columns-12 column-center">
				<?php if (function_exists('forge_page_navi')) { forge_page_navi(); } ?>
			</div>
		</div>
	</div>
<?php get_footer(); ?>