<?php get_header(); ?>
		
	<div class="row content-area">	

		
			
			<?php while ( have_posts() ) : the_post(); ?>
				<div class="columns-12">
					<?php echo wp_get_attachment_image(get_post_thumbnail_id( ),'full' ); ?>
					<h1 class="news-title"><?php the_title(); ?></h1>
				</div>
				<div class="columns-3">
					<p class="post-meta"><?php echo forge_saas_posted_on();  ?></p>
				</div>
				<div class="columns-7">
					<?php the_content(); ?>
				</div>
				
				<footer>

					<p class="tags"><?php the_tags('<span class="tags-title">Tags:</span> ', ' ', ''); ?></p>
					
				</footer> <!-- end article footer -->
				<?php endwhile; ?>
			

				

				


		
	</div>
	<div class="social-share-wrap">
		<div class="columns-6 column-center">

			<ul class="social-share">

				<p>Share this Article</p>

				<li><a class="ss-icon ss-social-regular twitter" href="https://twitter.com/intent/tweet?text=<?php the_title(); ?>&url=<?php the_permalink(); ?>" target=_"blank">twitter</a></li>

				<li><a class="ss-icon ss-social-regular facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target=_"blank">facebook</a></li>




				<!-- pinterest requires an image link to share. Usually we use the featured image. If that isn't being used, update the url here -->

				<?php 
					$thumb_id = get_post_thumbnail_id();
					$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
					$thumb_url = $thumb_url_array[0];
				?>


				<li><a class="ss-icon ss-social-regular email" href="mailto:insert%20email%20here?&subject=<?php bloginfo('name'); ?> Article - <?php the_title(); ?>&body=<?php the_permalink(); ?>">email</a></li>

			</ul>

		</div>
	</div>


<?php get_footer(); ?>