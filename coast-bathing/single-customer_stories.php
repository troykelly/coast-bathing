<?php get_header(); ?>
		
	<div class="story-first-callout">
		<div class="row">
			<div class="columns-10 column-center">
				<?php $img = wp_get_attachment_image(get_post_thumbnail_id( ),'full' ); ?>
				<?php echo $img; ?>
			</div>
			<div class="columns-10 right-1"><h1 class="story-intro"><?php the_title(); ?></h1></div>
			<div class="columns-3 right-1">
				<p class="story-subtitle"><?php the_field('sub_title'); ?></p>
			</div>
			<div class="columns-7 right-1">
				<div class="story-content">
					<?php while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="story-second-callout">
		<div class="row">
			<div class="columns-10 column-center">
				<h1 class="gallery-title"><?php the_field('story_single_gallery_title','options'); ?></h1>
				<?php if(have_rows('before_after')): ?>			
					<?php while(have_rows('before_after')): the_row(); ?>
						<div class="b-a">
							<div class="row">
								<div class="columns-6">
									<?php $img = wp_get_attachment_image(get_sub_field('before'),'stories' ); ?>
									<?php echo $img; ?>
									<?php if(get_sub_field('before_caption')): ?>
										<div class="caption"><p><?php the_sub_field('before_caption'); ?></p></div>
									<?php endif; ?>
								</div>
								<div class="columns-6">
									<?php $img = wp_get_attachment_image(get_sub_field('after'),'stories' ); ?>
									<?php echo $img; ?>
									<?php if(get_sub_field('after_caption')): ?>
										<div class="caption"><p><?php the_sub_field('after_caption'); ?></p></div>
									<?php endif; ?>
								</div>
							</div>
						</div>
					<?php endwhile; ?> 	
				<?php endif; ?>
			</div>
		 </div>
	</div>
<?php get_footer(); ?>