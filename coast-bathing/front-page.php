<?php
/*
 *  Template Name: Home Page
 */
get_header(); ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'templates/homepage', 'hero' ); ?>
		<?php get_template_part( 'templates/homepage', 'first-callout' ); ?>
		<?php get_template_part( 'templates/homepage', 'second-callout' ); ?>
		<?php get_template_part( 'templates/homepage', 'customer-story' ); ?>
		<?php get_template_part( 'templates/homepage', 'block-grid' ); ?>
		<?php get_template_part( 'templates/homepage', 'news' ); ?>
	<?php endwhile; // end of the loop. ?>	
<?php get_footer(); ?>