<?php get_header(); ?>


			<?php while ( have_posts() ) : the_post(); ?>

				<div class="page-intro">
				<div class="row">
					<div class="columns-10 right-1">
						<div class="page-title">
							<h1><?php the_title(); ?></h1>
						</div>
					</div>
					<div class="columns-3 right-1">
						<div class="sub-title">
							<p><?php the_field('sub_title'); ?></p>
						</div>
					</div>
					<div class="columns-7 right-1">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
			<?php endwhile; // end of the loop. ?>


<?php get_footer(); ?>