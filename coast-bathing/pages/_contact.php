<?php
/*
 * Template Name: Contact Page
 */
get_header(); ?>

	<div class="page-intro">
		<div class="row">
			<div class="columns-3 right-1">
				<div class="sub-title">
					<h2><?php the_title(); ?></h2>
				</div>
			</div>
			<div class="columns-7 right-1">
				<?php the_field('page_intro'); ?>
			</div>
		</div>
	</div>
	<?php $bg = wp_get_attachment_image_src(get_field('event_background'), 'full' ); ?>
	<div class="upcoming-events" style='background:url(<?php echo $bg[0];  ?>); background-size:cover;'>
		<div class="row">
			<div class="columns-10 column-center">
				<h1><?php the_field('event_title'); ?></h1>
				<a href="<?php the_field('event_link'); ?>" class="read-more"><?php the_field('event_link_text'); ?></a>
			</div>
		</div>
	</div>
	<?php $map = wp_get_attachment_image_src(get_field('map_background'), 'full' ); ?>
	<div class="map-callout" style='background:url(<?php echo $map[0];  ?>); background-size:cover;'>
		<div class="row">
			<div class="columns-6 right-6">
				<?php the_field('map_content'); ?>
			</div>
		</div>
	</div>
	<div class="contact-information">
		<div class="row">
			<div class="columns-3 right-1">
				<h2>Contact Information</h2>
			</div>
			<div class="columns-6 right-1">
				<?php if(have_rows('contact_information')): ?>
					<?php while(have_rows('contact_information')): the_row(); ?>
						<div class="location-wrap">
							<div class="row">
								<div class="columns-12">
									<h5><?php the_sub_field('title'); ?></h5>
								</div>
								<?php if(get_sub_field('location')): ?>
									<div class="columns-6">
										<div class="location"><?php the_sub_field('location'); ?></div>
										<a href="<?php the_sub_field('directions_link'); ?>" class="read-more">Directions</a>
									</div>
								<?php endif; ?>
								<div class="columns-6">
									<p class="phone-wrap"><span class="phone">Phone </span> <?php the_sub_field('phone'); ?></p>
									<p><span class="email">Email </span> <?php the_sub_field('email'); ?></p>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	
	</div>
	<?php $ref = wp_get_attachment_image_src(get_field('referral_background'), 'full' ); ?>
	<div class="referral-program" style='background:url(<?php echo $ref[0];  ?>); background-size:cover;'>
		<div class="row">
			<div class="columns-10 column-center">
				<h1><?php the_field('referral_title'); ?></h1>
				<a href="<?php the_field('referral_link'); ?>" class="read-more"><?php the_field('referral_link_text'); ?></a>
			</div>
		</div>
	</div>
	<div class="form">
		<div class="row">
			<div class="columns-3 right-1">
				<div class="sub-title">
					<h2><?php the_field('form_title'); ?></h2>
				</div>
			</div>
			<div class="columns-7 right-1">
				<div class="form-intro">
					<p><?php the_field('form_intro'); ?></p>
				</div>
				<div class="form-content">
					<?php the_field('contact_form'); ?>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>