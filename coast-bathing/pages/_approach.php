<?php
/*
 * Template Name: Approach Page
 */
get_header(); ?>

	<div class="page-intro">
		<div class="row">
			<div class="columns-10 right-1">
				<div class="page-title">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
			<div class="columns-3 right-1">
				<div class="sub-title">
					<p><?php the_field('sub_title'); ?></p>
				</div>
			</div>
			<div class="columns-7 right-1">
				<?php the_field('page_intro'); ?>
			</div>
		</div>
	</div>
	<?php $count = 0; ?>
	<?php if(have_rows('approach_repeater')): ?>
	<?php while(have_rows('approach_repeater')): the_row(); ?>
	<?php $count++; ?>
		<?php if($count%2 != 0): ?>
			<div class="approach-wrap blue">
				<div class="special-row row">
					<?php $bg = wp_get_attachment_image_src(get_sub_field('image'), 'full' ); ?>
					<div class="columns-5" style="background:url(<?php echo $bg[0]; ?>); background-size:cover;">&nbsp;</div>
					<div class="columns-7">
						<div class="blue-content">
							<?php the_sub_field('content'); ?>
							<?php if(get_sub_field('link')): ?>
								<a href="<?php the_sub_field('link'); ?>" class="read-more"><?php the_sub_field('link_text'); ?></a>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		<?php else: ?>
			<div class="approach-wrap white">
				<div class="special-row row">
					<?php $bg = wp_get_attachment_image_src(get_sub_field('image'), 'full' ); ?>
					<div class="columns-5 right-7" style="background:url(<?php echo $bg[0]; ?>); background-size:cover;">&nbsp;</div>
					<div class="columns-7 left-5">
						<div class="white-content">
							<?php the_sub_field('content'); ?>
							<?php if(get_sub_field('link')): ?>
								<a href="<?php the_sub_field('link'); ?>" class="read-more"><?php the_sub_field('link_text'); ?></a>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
	<?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>