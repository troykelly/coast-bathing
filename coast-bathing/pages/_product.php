<?php
/*
 * Template Name: Product Page
 */
get_header(); ?>
	<div class="product-first-section">
		<div class="row">
			<div class="columns-3 right-1">
				<h1><?php the_title(); ?></h1>
				<p class="product-intro"><?php the_field('page_intro'); ?></p>
			</div>
			<div class="columns-7 right-1 ">
				<div class="slider-wrapper">
					<div class="flexslider product-slider">
						<ul class="slides">
							<?php foreach(get_field('image_galleries') as $gallery): ?>
							<li>
								<div class="img-bg" style="background-image: url(<?php echo $gallery['url'];?>);"></div>
							</li>
							<?php endforeach; ?>
						</ul>
					</div>

					<ul class="block-grid-6 gallery-control">
						<?php foreach(get_field('image_galleries') as $gallery): ?>
						<li>
							<div class="img-bg" style="background-image: url(<?php echo $gallery['url'];?>);"></div>
						</li>
						<?php endforeach; ?>
					</ul>
				</div>
				<div class="callout-wrapper">
					<?php while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; ?>
					<div class="repeater-wrapper">
						<?php if(have_rows('types_of_product')): ?>
							<h5><?php the_field('repeater_title'); ?></h5>
							<ul class="block-grid-2">
								<?php while(have_rows('types_of_product')): the_row(); ?>
									<li><p><?php the_sub_field('type'); ?></p></li>
								<?php endwhile; ?>
							</ul>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="second-callout">
		<div class="row">
			<div class="columns-6 column-center">
				<h1 class="second-callout-title"><?php the_field('second_callout_title'); ?></h1>
				<div class="subtitle"><h4><?php the_field('second_callout_subtitle'); ?></h4></div>
				<div class="second-intro"><?php the_field('second_callout_intro'); ?></div>
				<div class="row">
					<div class="columns-6">
						<div class="second-col-1">
							<?php the_field('second_callout_column_1'); ?>
						</div>
					</div>
					<div class="columns-6">
						<div class="second-col-2">
							<?php the_field('second_callout_column_2'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="customer-story-callout">
		<?php 

		$posts = get_field('customer_story');

		if( $posts ): ?>
			<?php foreach( $posts as $post ): // variable must NOT be called $post (IMPORTANT) ?>
			<?php setup_postdata($post ); ?>	
				<div class="row">
				   <div class="columns-6">
				   		<?php echo wp_get_attachment_image(get_post_thumbnail_id( ),'story' ); ?>
				   </div>
					<div class="columns-6">
						<div class="story-content">
							<h6>Customer Story</h6>
							<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
							<p><?php echo excerpt(20); ?></p>
							<a href="<?php the_permalink(); ?>" class="read-more">Read Story</a>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
			<?php wp_reset_postdata(); ?>
		<?php endif; ?>

	</div>
		<script>

		jQuery(document).ready(function($) {

			$('.tab-slider').flexslider({
				animation: "fade",
				manualControls: ".slider-thumbs li",
				animationSpeed: 1,
				directionNav: false,
				slideshow: false
			});

		});

	</script>
<?php get_footer(); ?>