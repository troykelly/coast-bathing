<?php
/*
 * Template Name: Financing Page
 */
get_header(); ?>

	<div class="page-intro">
		<div class="row">
			<div class="columns-10 right-1">
				<div class="page-title">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
			<div class="columns-3 right-1">
				<div class="sub-title">
					<p><?php the_field('sub_title'); ?></p>
				</div>
			</div>
			<div class="columns-7 right-1">
				<?php the_field('page_intro'); ?>
			</div>
		</div>
	</div>
	<div class="financing-options">
		<div class="row">
			<div class="columns-12">
				<div class="financing-title">
					<h1><?php the_field('financing_title'); ?></h1>
				</div>
			</div>
		</div>
		<?php $options = get_field('financing_options'); ?>
		<?php if($options): ?>
			<?php foreach ($options as $post): ?>
				<?php setup_postdata($post); ?>
				<div class="option">
					<div class="row">
						<div class="columns-2 right-2">
							<?php $icon = wp_get_attachment_image(get_field('page_icon'), 'team' ); ?>
							<?php echo $icon; ?>
						</div>
						<div class="columns-7 right-2">
							<div class="option-content">
								<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<p><?php echo excerpt(20); ?></p>
								<a href="<?php the_permalink(); ?>" class="read-more">Learn More</a>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
			<?php wp_reset_postdata(); ?>	
		<?php endif; ?>
	</div>
<?php get_footer(); ?>