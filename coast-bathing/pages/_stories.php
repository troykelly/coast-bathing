<?php
/*
 * Template Name: Stories Page
 */
get_header(); ?>

	<div class="page-intro">
		<div class="row">
			<div class="columns-10 right-1">
				<div class="page-title">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
			<div class="columns-3 right-1">
				<div class="sub-title">
					<p><?php the_field('sub_title'); ?></p>
				</div>
			</div>
			<div class="columns-7 right-1">
				<?php the_field('page_intro'); ?>
			</div>
		</div>
	</div>
	<?php
			$args = array(
				'post_type' => 'customer_stories',
				'posts_per_page' => 6,
				'order' => 'ASC',
				'orderby' => 'menu_order'
				);
			
			$second_query = new WP_Query($args);?>
			
			<?php if($second_query->have_posts()): ?> 
			<div class="row">
				<div class="columns-10 column-center">
					<ul class="block-grid-2 stories">
						<?php while($second_query->have_posts()): $second_query->the_post();?>
							<li class="story">
								<?php $featured = wp_get_attachment_image_src(get_post_thumbnail_id( ), 'stories' ); ?>
								<div class="story-wrap">
									<a href="<?php the_permalink(); ?>" class="hovered">
										<img src="<?php echo $featured[0]; ?>" alt="">
										<div class="button">Read Story</div>	
									</a>
								</div>	
								<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>	
								<p><?php echo excerpt(25); ?></p>		
								<a href="<?php the_permalink(); ?>" class="read-more">Read Story</a>
							</li>
						<?php endwhile; ?>
					</ul>
				</div>	
			</div>
			<?php endif; ?>
			<div class="navigation">

				<div class="row">
					<div class="columns-12">
						<?php forge_page_navi(); ?>
					</div>
				</div>
				
			</div>

<?php get_footer(); ?>