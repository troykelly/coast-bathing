<?php
/*
 * Template Name: About Page
 */
get_header(); ?>

	<div class="page-intro">
		<div class="row">
			<div class="columns-10 right-1">
				<div class="page-title">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
			<div class="columns-3 right-1">
				<div class="sub-title">
					<p><?php the_field('sub_title'); ?></p>
				</div>
			</div>
			<div class="columns-7 right-1">
				<?php the_field('page_intro'); ?>
			</div>
		</div>
	</div>
	<div class="team-wrap">
		
		<div class="row">
			<div class="columns-12">	
				<h1 class="team-title"><?php the_field('team_title'); ?></h2>
			</div>
		</div>

		<?php
			$args = array(
				'post_type' => 'team',
				'posts_per_page' => -1,
				'order' => 'ASC',
				'orderby' => 'menu_order'
			);
		
			$second_query = new WP_Query($args);?>
		
			<?php if($second_query->have_posts()): ?> 
				<?php while($second_query->have_posts()): $second_query->the_post();?>
					<div class="team-member">
						<div class="row">
							<div class="columns-3 right-1">
								<?php $image = wp_get_attachment_image(get_post_thumbnail_id( ), 'team' ); ?>
								<div class="team-pic">
									<div class="shadow"></div>
									<?php echo $image; ?>
								</div>
							</div>
							<div class="columns-7 right-1">
								<div class="team-content-wrap">
									<h2><?php the_title(); ?></h2>
									<h6><?php the_field('job_title'); ?></h6>
									<?php the_content(); ?>
								</div>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
	</div> 

<?php get_footer(); ?>