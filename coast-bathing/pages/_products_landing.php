<?php
/*
 * Template Name: Products Landing Page
 */
get_header(); ?>

	<div class="page-intro">
		<div class="row">
			<div class="columns-10 right-1">
				<div class="page-title">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
			<div class="columns-3 right-1">
				<div class="sub-title">
					<p><?php the_field('sub_title'); ?></p>
				</div>
			</div>
			<div class="columns-7 right-1">
				<?php the_field('page_intro'); ?>
			</div>
		</div>
	</div>
	<?php $options = get_field('product_options'); ?>
	<?php if($options): ?>
		<div class="option-wrap">
			<div class="row">
				<div class="columns-12">	
					<h2 class="products-title"><?php the_field('products_option_title'); ?></h2>
				</div>
			</div>
			<div class="row">
				<div class="columns-12">
					<ul class="block-grid-3">
						<?php foreach ($options as $post): ?>
							<?php setup_postdata($post); ?>
							<li class="product">
								<div class="product-icon">
									<?php $icon = wp_get_attachment_image(get_field('page_icon'), 'team' ); ?>
									<?php echo $icon; ?>
								</div>
								<div class="product-content">
									<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
									<p><?php echo excerpt(20); ?></p>
									<a href="<?php the_permalink(); ?>" class="button">Learn More</a>
								</div>
							</li>
						<?php endforeach; ?>
					</ul>
					<?php wp_reset_postdata(); ?>
				</div>
			</div>
		</div>	
	<?php endif; ?>
<?php get_footer(); ?>