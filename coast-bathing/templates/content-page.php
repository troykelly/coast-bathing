<?php
/*
 * The template used for displaying page content in page.php
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		
		<h1 class="entry-title"><?php the_title(); ?></h1>
	
	</header><!-- .entry-header -->

	<?php the_content(); ?>

</article><!-- #post-## -->