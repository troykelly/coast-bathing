

<?php 
    $banner_video = get_field('mp4');
    $video_placeholder = wp_get_attachment_image_src(get_field('fallback_image'),'full' );
   
?>

<div class="homepage-section homepage-banner" style="background:url(<?php echo $video_placeholder[0]; ?>); background-size:cover;">
	<video id="bg-video" autoplay>
		<source src="<?php echo $banner_video; ?>" type="video/mp4">
	</video> 

	<div class="hero-content">
		<h3><?php the_field('hero_line_1'); ?></h3>
		<h5><?php the_field('hero_line_2'); ?></h5>
		<h4><?php the_field('hero_line_3'); ?></h4>
	</div>
</div>
<script>
	$('#bg-video').on('ended', function(){
		$('#bg-video').fadeOut(3000);
	});
	
	
</script>

