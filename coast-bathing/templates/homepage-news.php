<div class="homepage-section homepage-news">
<div class="homepage-news-title">
	<h1>Recent News</h1>
</div>
	<?php $news = get_latest('post', 2); ?>
		<?php while ($news -> have_posts()) : $news->the_post(); ?>
		
			<div class="row">
				<div class="columns-4 right-2">
					<div class="news-image ">
						<?php echo wp_get_attachment_image(get_post_thumbnail_id(), 'grid' ); ?>
					</div>
				</div>
				<div class="columns-5 right-2">
					<div class="news-content">
						<p class="post-meta"><?php echo forge_saas_posted_on();  ?></p>
						<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
						<p><?php echo excerpt(30); ?></p>
						<a href="<?php the_permalink(); ?>" class="read-more">Read Article</a>
					</div>
				</div>
			</div>
		<?php endwhile; ?>
	<?php wp_reset_query(); ?>

	<div class="blog-link">
		<a href="<?php the_field('blog_link'); ?>" class="button">View All News</a>
	</div>
	
</div>