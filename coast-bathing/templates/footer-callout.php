<div class="footer-section footer-preview">
	<div class="special-row row">
		<?php $bg1 = wp_get_attachment_image_src(get_field('preview_background_1', 'options'),'full' ); ?>
		<div class="columns-5" style="background:url(<?php echo $bg1[0]; ?>); background-size:cover; background-position:center; background-repeat: no-repeat;">
			<div class="footer-header-link-wrap">
				<div class="preview-header">
					<h3><?php the_field('preview_header', 'options'); ?></h3>
				</div>
				<div class="preview-links">
					<?php if(get_field('link_1','options')): ?>
						<?php $term = get_field('link_1','options'); ?>
						<a href="<?php echo get_term_link($term); ?>" class="read-more-white"><?php the_field('link_1_text', 'options'); ?></a>
					<?php endif; ?>
					<?php if(get_field('link_2','options')): ?>
						<a href="<?php the_field('link_2', 'options'); ?>" class="read-more-white"><?php the_field('link_2_text', 'options'); ?></a>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<?php $bg2 = wp_get_attachment_image_src(get_field('preview_bg_2', 'options'),'full' ); ?>
		<div class="columns-7" style="background:url(<?php echo $bg2[0]; ?>); background-size:cover; background-position:center; background-repeat: no-repeat;">
			<div class="preview-content">
				<p><?php the_field('preview_content', 'options'); ?></p>
				<a href="<?php the_field('preview_content_link', 'options'); ?>" class="button"><?php the_field('preview_content_link_text', 'options'); ?></a>
			</div>
		</div>
	</div>
</div>
