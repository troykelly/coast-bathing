<div class="homepage-section homepage-block-grid">
	<div class="row">
		<div class="columns-6 column-center">
			<div class="grid-intro">
				<?php the_field('grid_intro'); ?>
			</div>
		</div>
	</div>
	<div class="grid-wrapper">
		<div class="row">
			<div class="columns-12">
				<?php if(have_rows('block_grid')): ?>
					<ul class="block-grid-3">
						<?php while(have_rows('block_grid')): the_row(); ?>
							<li>
								<div class="image-wrap">
									<?php echo wp_get_attachment_image(get_sub_field('image'), 'grid' ); ?>
									<div class="shadow"></div>
								</div>
								<div class="grid-content">
									<?php the_sub_field('content'); ?>
								</div>
							</li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
				<a href="<?php the_field('grid_link'); ?>" class="button"><?php the_field('grid_link_text'); ?></a>
			</div>
		</div>

	</div>
</div>

