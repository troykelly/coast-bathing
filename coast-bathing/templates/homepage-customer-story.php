<?php 

$posts = get_field('customer_story');

if( $posts ): ?>
	<?php foreach( $posts as $post ): // variable must NOT be called $post (IMPORTANT) ?>
	<?php setup_postdata($post ); ?>
	<div class="homepage-section homepage-customer-story">
	
		<div class="row">
		   <div class="columns-6">
		   		<?php echo wp_get_attachment_image(get_post_thumbnail_id( ),'story' ); ?>
		   </div>
			<div class="columns-6">
				<div class="story-content">
					<h6>Customer Story</h6>
					<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
					<p><?php echo excerpt(20); ?></p>
					<a href="<?php the_permalink(); ?>" class="read-more">Read Story</a>
				</div>
			</div>
		</div>

	</div>
	<?php endforeach; ?>
	<?php wp_reset_postdata(); ?>
<?php endif; ?>
