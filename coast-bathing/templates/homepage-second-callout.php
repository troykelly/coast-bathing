<div class="homepage-section homepage-second-callout">
	<div class="row">
		<div class="columns-6 column-center">
			<div class="callout-wrap ">
				<div class="callout-title">
					<?php the_field('second_callout_title'); ?>
				</div>
				<div class="callout-content">
					<?php the_field('second_callout_content'); ?>
					<a href="<?php the_field('second_callout_link'); ?>" class="read-more"><?php the_field('second_callout_link_text'); ?></a>
				</div>
			</div>
		</div>
	</div>
</div>