<?php

// replace Gravity Form submit inputs to buttons
add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );
function form_submit_button( $button, $form ) {
    return "<button class='button gf_submit' id='gform_submit_button_{$form['id']}'>{$form['button']['text']}</button>";
}


// turn Gravity Forms CSS off when Anvil is activated
function gravity_css_off_theme_activate() {
	update_option( 'rg_gforms_disable_css', '1' );
	update_option( 'rg_gforms_enable_html5', '1' );
}
add_action('after_switch_theme','gravity_css_off_theme_activate');


// turn Gravity Forms CSS off when the Gravity Forms plugin is activate
function gravityform_activate() {
	update_option( 'rg_gforms_disable_css', '1' );
	update_option( 'rg_gforms_enable_html5', '1' );
}
register_activation_hook( WP_PLUGIN_DIR . '/gravityforms/gravityforms.php' ,'gravityform_activate');