<?php 
/*	OPTIONS_PAGE.PHP

	This page should be used to register extra options pages for
	advanced custom fields.

	EXAMPLE:

	register_options_page('Name');
	
*/

if(function_exists('acf_add_options_page')) {

	acf_add_options_page();
	acf_add_options_sub_page('Contact');
	acf_add_options_sub_page('Header');
	acf_add_options_sub_page('Footer');

	// add a 404 setting under the page menu
	acf_add_options_sub_page( 
		array(
			'title' => '404 Page' ,
			'parent' => 'edit.php?post_type=page'
		)
	);
}

register_activation_hook( WP_PLUGIN_DIR . '/advanced-custom-fields-pro/acf.php' ,'forge_import_404_option_acfs');
function forge_import_404_option_acfs() {

	// give it a hard coded earily check, so don't need to read file
	if( acf_get_field_group('group_5579f28acfa5c', true) ) return;

	$json = file_get_contents( get_stylesheet_directory().'/acf-404-option-page.json' );
	$json = json_decode($json, true);

	$ref = array();
	$order = array();
	$default_404_options = array();

    foreach( $json as $field_group ) {

    	if( acf_get_field_group($field_group['key'], true) ) continue;
	    	
		$fields = acf_extract_var($field_group, 'fields');
		$fields = acf_prepare_fields_for_import( $fields );
		$field_group = acf_update_field_group( $field_group );
		$ref[ $field_group['key'] ] = $field_group['ID'];
		$order[ $field_group['ID'] ] = 0;
		
		foreach( $fields as $field ) {
			if( empty($field['parent']) ) {
				$field['parent'] = $field_group['ID'];
			} elseif( isset($ref[ $field['parent'] ]) ) {
				$field['parent'] = $ref[ $field['parent'] ];
			}

			if( !isset($order[ $field['parent'] ]) ) {
				$order[ $field['parent'] ] = 0;
			}
			
			$field['menu_order'] = $order[ $field['parent'] ];
			$order[ $field['parent'] ]++;
			
			$field = acf_update_field( $field );
			$ref[ $field['key'] ] = $field['ID'];

			$default_404_options[] = $field;
		}
	}

	foreach( $default_404_options as $option ) {
		if( get_field($option['name'], 'options') !== null ) continue;
		update_field($option['key'], $option['default_value'], 'options' );
	}
}
