<?php 
/**
 * Forge Saas Custom Post Types
 *
 * @package Forge Saas
 */

// let's create the function for the custom type
function forge_register_post_types() { 

	// pass in an array of post types. Define their post slug and labels
	$the_post_types = array(
		array( 
			'post_slug' 	=>	'customer_stories', 
			'post_label' 	=>	'Customer Stories', 
			'post_single' 	=>	'Customer Story',
			'rewrite_slug'	=>  'our-customer-stories'
		),
		array( 
			'post_slug' 	=>	'team', 
			'post_label' 	=>	'Team', 
			'post_single' 	=>	'Team',
			'rewrite_slug'	=>  'our-team'
		),
		// array( 
		// 	'post_slug' 	=>	'testimonials', 
		// 	'post_label' 	=>	'Testimonials', 
		// 	'post_single' 	=>	'Testimonial',
		// 	'rewrite_slug'	=>  'our-testimonials'
		// ),
		// array( 
		// 	'post_slug' 	=>	'careers', 
		// 	'post_label' 	=>	'Careers', 
		// 	'post_single' 	=>	'Career',
		// 	'rewrite_slug'	=>  'our-careers'
		// )
	); 

	// loop through post type array and register post types
	foreach($the_post_types as $post_type){

		$default = array(
			'labels' => array(
				/* cpt general */
				'name' 					=> __( $post_type['post_label'], 'anvil' ), 				/* This is the Title of the Group */
				'singular_name' 		=> __( $post_type['post_single'], 'anvil' ), 				/* This is the individual type */
				'add_new' 				=> __( 'Add New', 'anvil' ), 								/* The add new menu item */
				'add_new_item'			=> __( 'Add New '.$post_type['post_single'], 'anvil' ), 	/* Add New Display Title */
				'edit'					=> __( 'Edit', 'anvil' ), 								/* Edit Dialog */
				'edit_item'				=> __( 'Edit '.$post_type['post_single'], 'anvil' ), 		/* Edit Display Title */
				'new_item'				=> __( 'New '.$post_type['post_single'], 'anvil' ), 		/* New Display Title */
				'view_item'				=> __( 'View '.$post_type['post_single'], 'anvil' ), 		/* View Display Title */
				'search_items'			=> __( 'Search '.$post_type['post_single'], 'anvil' ), 	/* Search Custom Type Title */ 
				'not_found'				=> __( 'Nothing found in the Database.', 'anvil' ), 		/* This displays if there are no entries yet */ 
				'not_found_in_trash'	=> __( 'Nothing found in Trash', 'anvil' ), 				/* This displays if there is nothing in the trash */
				'parent_item_colon'		=> __( 'Nothing found in Trash', 'anvil' ),				/* This displays the parent text if hierarchical */

				/* feature images label */
				// 'featured_image'		=> __( 'Featured Image', 'anvil' ),
				// 'set_featured_image'	=> __( 'Set featured image', 'anvil' ),
				// 'remove_featured_image'	=> __( 'Remove featured image', 'anvil' ),
				// 'use_featured_image'	=> __( 'Use as featured image', 'anvil' ),

				/* media manager */
				'insert_into_item'		=> __( 'Insert into '.$post_type['post_single'], 'anvil' ),
				'uploaded_to_this_item' => __( 'Uploaded to this '.$post_type['post_single'], 'anvil' ),

			), /* end of arrays */
			// 'menu_icon' 			=> '',
			'public' 				=> true,
			// 'publicly_queryable'	=> true,
			// 'exclude_from_search'	=> false,
			// 'show_ui' 				=> true,
			'query_var' 			=> true,
			'has_archive'			=> false,
			'menu_position'			=> 5, /* this is what order you want it to appear in on the left hand side menu */ 
			'rewrite'				=> array('with_front' => false, 'slug' => $post_type['rewrite_slug']),
			'capability_type'		=> 'post',
			'hierarchical'			=> false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports'				=> array( 'title', 'editor', 'thumbnail', 'revisions','excerpt')
		);
	
		$default['labels'] = isset($post_type['labels'])? wp_parse_args($post_type['labels'], $default['labels']) : $default['labels'];
		if( isset($post_type['labels']) ) unset($post_type['labels']);

		$args = wp_parse_args( $post_type, $default );

		/* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		register_post_type( $post_type['post_slug'], $args);

	}

	// array used to register taxonomies for post types. 
	$taxonomy_types = array(
		// array(
		// 	'tax_slug'		=>	'people-types',
		// 	'tax_label'		=>	'People Types',
		// 	'tax_single'	=>	'People Type',
		// 	'post_type'		=>	'people',
		// 	'rewrite_slug'	=>	'people-types'
		// ),
		// array(
		// 	'tax_slug'		=>	'service-types',
		// 	'tax_label'		=>	'Service Types',
		// 	'tax_single'	=>	'Service Type',	
		// 	'post_type'		=>	'services',
		// 	'rewrite_slug'	=>	'service-types'
		// ) 
	
	);

	// loop through $taxonomy_types and register taxonomies. 
	foreach($taxonomy_types as $taxonomy){
		
		$default = array(
			'hierarchical' => true,     /* if this is true it acts like categories */             
    		'labels' => array(
    			'name' 				=> __( $taxonomy['tax_label'] ), /* name of the custom taxonomy */
    			'singular_name' 	=> __( $taxonomy['tax_single'] ), /* single taxonomy name */
    			'search_items'		=> __( 'Search '.$taxonomy['tax_label'] ), /* search title for taxomony */
    			'all_items'			=> __( 'All '.$taxonomy['tax_label'] ), /* all title for taxonomies */
    			'parent_item'		=> __( 'Parent '.$taxonomy['tax_single'] ), /* parent title for taxonomy */
    			'parent_item_colon' => __( 'Parent '.$taxonomy['tax_single'].':' ), /* parent taxonomy title */
    			'edit_item'			=> __( 'Edit '.$taxonomy['tax_single'] ), /* edit custom taxonomy title */
    			'update_item'		=> __( 'Update '.$taxonomy['tax_single'] ), /* update title for taxonomy */
    			'add_new_item'		=> __( 'Add New '.$taxonomy['tax_single'] ), /* add new title for taxonomy */
    			'new_item_name'		=> __( 'New '.$taxonomy['tax_single'].' Name' ) /* name title for taxonomy */
    		),
    		'show_ui' 	        => true,
    		'query_var'         => true,
    		'show_admin_column' => true,
    		'rewrite'           => array( 'slug' => $taxonomy['rewrite_slug'] ),
		);

		$args = wp_parse_args( $taxonomy, $default );

		register_taxonomy( $taxonomy['tax_slug'], $taxonomy['post_type'], $args);  
		
	}
	
} 

	// adding the function to the Wordpress init
	add_action( 'init', 'forge_register_post_types');
