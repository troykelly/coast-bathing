<?php

// Page Excerpts
// ===================================================================
add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
	add_post_type_support( 'page', 'excerpt' );
}




// Query Functions
// ===================================================================


/**
 * get_query function.
 *
 * @param  string $post_type. The post type you'd like to pull fron, default: 'post'
 * @param  int $limit. How many results do you want, default: -1
 * @param  array/string $args. Overwrite wp_query's arg, default: ''
 * @return obj
 *
 */
function get_query( $post_type = 'post', $limit = null, $args = '') {
	$limit = is_null($limit)? get_option('posts_per_page') : $limit;
	$paged = get_query_var('paged')? get_query_var('paged') : 1;
	$defaults = array(
		'post_type' => $post_type,
		'posts_per_page' => $limit,
		// 'order' => 'ASC',
		// 'orderby' => 'menu_order', // they have a backend option setting now, don't need to do this
		'paged' => $paged
	);

	$args = wp_parse_args( $args, $defaults );

	return new WP_Query( $args );
}

/* return a wp_query object with random order (shortcut) */
function get_random( $post_type = 'post', $limit = null, $args = '') {
	$default = array(
		'orderby' => 'rand'
	);
	return get_query( $post_type, $limit, wp_parse_args( $args, $default ));
}

/* return a wp_query object with date order (shortcut) */
function get_latest( $post_type = 'post', $limit = null, $args = '') {
	$default = array(
		'orderby' => 'date',
		'order' => 'desc'
	);
	return get_query( $post_type, $limit, wp_parse_args( $args, $default ));
}

/* return a wp_query object with related (shortcut) */
function get_related( $limit = null, $related_by = 'category', $exclude_realted_ids = array() ) {
    $current_id = get_the_ID();
    $post_type = get_post_type();

    $the_terms = get_the_terms( get_the_ID(), $related_by );
    $term_ids = array();

    if( $the_terms ) {
        foreach( $the_terms as $tt ) {
            if( in_array( $tt->term_id, $exclude_realted_ids ) ) continue;
            $term_ids[] = $tt->term_id;
        }
    }

    $args = array(
        'post__not_in' => array( $current_id ),
        'tax_query' => array(
            array(
                'taxonomy' => $related_by,
                'field'    => 'term_id',
                'terms'    => $term_ids,
            ),
        ),
    );
    return get_random( $post_type, $limit, $args);
}


// IE8 Template
// ====================================================================
/**
 * no_more_ie_eight function.
 *
 * Detect the browser user agent, if it's ie 2-8, block the site
 * and show the ie8 template.
 *
 * Event 9, 10 should be auto updated, but we shouldn't block the user
 * from using the site.
 *
 */
// add_filter( 'template_include', 'no_more_ie_eight', 15);
function no_more_ie_eight( $template ) {
	// important to not include 1, because ie 10 11 1121223 895340y*&@#)$*($)
	if( preg_match('/(?i)msie [2-8]/',$_SERVER['HTTP_USER_AGENT'] ) ){
		$template = get_query_template('ie8');
	}
	return $template;
}


// Wysiwyg Content Wrapper
// ====================================================================
/**
 * Warp "get_content()" with a div for styling
 *
 * As well as ACF's wysiwyg fields
 *
 */

add_filter( 'the_content', 'forge_content_wrapper', 50 );
add_filter( 'acf_the_content', 'forge_content_wrapper', 50 );

function forge_content_wrapper( $content ) {
	return '<div class="entry-content">'.$content.'</div>';
}


// WP Remove "Large" image size (wp backend uses "medium", so can't remove)
// ====================================================================
add_filter('intermediate_image_sizes', 'forge_remove_large_image_size');
function forge_remove_large_image_size( $sizes ) {
	// if (in_array('medium', $sizes)) unset($sizes[array_search('medium', $sizes)]);
	if (in_array('large', $sizes)) unset($sizes[array_search('large', $sizes)]);

	return $sizes;
}

add_filter('acf/get_image_sizes', 'forge_remove_large_image_size'); // acf hardcoded....
function forge_acf_remove_large_image_size( $sizes ) {
	// if (in_array('medium', $sizes)) unset($sizes[array_search('medium', $sizes)]);
	if (isset($sizes['large'])) unset($sizes['large']);

	return $sizes;
}


// WP Head Clean up
// ====================================================================
/**
 * don't show the wp generator tag
 */
remove_action( 'wp_head', 'wp_generator' );
