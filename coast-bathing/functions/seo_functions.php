<?php

class Blog_SEO_Rewrite {

	public $blog_url 			= 'blog'; // no trailing slash

	public $rewrite_archive 	= true;

	public $rewrite_day 		= true;
	public $redirect_day 		= true;

	public $rewrite_month 		= true;
	public $redirect_month 		= true;

	public $rewrite_year 		= true;
	public $redirect_year 		= true;

	public $rewrite_category 	= true;
	public $redirect_category 	= true;

	public $rewrite_tag 		= true;
	public $redriect_tag 		= true;

	public $rewrite_author 		= true;
	public $redirect_author 	= true;

	public $rewrite_single 		= true;
	public $redirect_single 	= true;

	// ====================================

	public function __construct() {

		if( get_option('permalink_structure') ) {

			$this->blog_url = apply_filters( 'blog_seo_rewrite:blog_url', $this->blog_url );

			do_action( 'blog_seo_rewrite:before_init', $this );

			add_action( 'init', 				array( $this, 'init' ) );
			add_action( 'template_redirect', 	array( $this, 'template_redirect' ) );

		}

	}
	
	public function init() {

		if( $this->rewrite_archive ) {
			$this->rewrite_archive_url();
		}

		if( $this->rewrite_day ) { 
			$this->rewrite_day();		
			add_filter( 'day_link', array( $this, 'modify_day_link' ), 20, 4 );
		}

		if( $this->rewrite_month ) { 
			$this->rewrite_month();		
			add_filter( 'month_link', array( $this, 'modify_month_link' ), 20, 3 );
		}

		if( $this->rewrite_year ) { 
			$this->rewrite_year();		
			add_filter( 'year_link', array( $this, 'modify_year_link' ), 20, 2 );
		}

		if( $this->rewrite_category ) { 
			$this->rewrite_category_url();
			add_filter( 'category_link', array( $this, 'modify_category_link' ), 20 );
		}

		if( $this->rewrite_tag ) { 
			$this->rewrite_tag_url();
			add_filter( 'tag_link', array( $this, 'modify_tag_link' ), 20 );
		}

		if( $this->rewrite_author ) { 
			$this->rewrite_author_url();
			add_filter( 'author_link', array( $this, 'modify_author_link' ), 20 );
		}

		if( $this->rewrite_single ) { 
			$this->rewrite_single_url();
			add_filter( 'post_link', array( $this, 'modify_single_link' ), 20, 2 );
		}

	}
	
	public function template_redirect() {

    	if( get_post_type() == 'post' && strpos( $_SERVER["REQUEST_URI"], "/{$this->blog_url}/" ) === false ) {


			if( is_day() 		&& $this->rewrite_day 		&& $this->redirect_day 		) 		$this->redirect_day_url();

			if( is_month() 		&& $this->rewrite_month 	&& $this->redirect_month 	) 		$this->redirect_month_url();

			if( is_year() 		&& $this->rewrite_year 		&& $this->redirect_year 	) 		$this->redirect_year_url();

			if( is_category() 	&& $this->rewrite_category 	&& $this->redirect_category ) 		$this->redirect_category_url();

			if( is_tag() 		&& $this->rewrite_tag 		&& $this->redirect_tag 		) 		$this->redirect_tag_url();

			if( is_author() 	&& $this->rewrite_author 	&& $this->redirect_author 	) 		$this->redirect_author_url();

			if( is_single() 	&& $this->rewrite_single 	&& $this->redirect_single 	)	 	$this->redirect_single_url();
		}
		
	}

	// Day ==========================
	public function rewrite_day() {

		add_rewrite_rule(
			sprintf('^%s/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/([0-9]{1,})/?$', $this->blog_url),
			'index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]', 
			'top'
		);

		add_rewrite_rule(
			sprintf('^%s/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$', $this->blog_url),
			'index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]', 
			'top'
		);

	}

	public function modify_day_link( $daylink, $year, $month, $day ) {

		$month = zeroise(intval($month), 2);
		$day = zeroise(intval($day), 2);

		$daylink = str_replace( 
			sprintf( '/%s/%s/%s/', $year, $month, $day ), 
			sprintf( '/%s/%s/%s/%s/', $this->blog_url, $year, $month, $day ), 
			$daylink 
		);

		return $daylink;
	}


	public function redirect_day_url() {

		$day_link = get_day_link( get_query_var( 'year' ), get_query_var( 'monthnum' ), get_query_var( 'day' ) );
    	wp_redirect( $day_link , 301 );
		die();

	}

	// Month ==========================
	public function rewrite_month() {

		add_rewrite_rule(
			sprintf('^%s/([0-9]{4})/([0-9]{1,2})/page/([0-9]{1,})/?$', $this->blog_url),
			'index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]', 
			'top'
		);

		add_rewrite_rule(
			sprintf('^%s/([0-9]{4})/([0-9]{1,2})/?$', $this->blog_url),
			'index.php?year=$matches[1]&monthnum=$matches[2]', 
			'top'
		);

	}

	public function modify_month_link( $monthlink, $year, $month ) {

		$month = zeroise(intval($month), 2);

		$monthlink = str_replace( 
			sprintf( '/%s/%s/', $year, $month ), 
			sprintf( '/%s/%s/%s/', $this->blog_url, $year, $month ), 
			$monthlink 
		);

		return $monthlink;
	}


	public function redirect_month_url() {

		$month_link = get_month_link( get_query_var( 'year' ), get_query_var( 'monthnum' ) );
    	wp_redirect( $month_link , 301 );
		die();

	}

	// Year ==========================
	public function rewrite_year() {

		add_rewrite_rule(
			sprintf('^%s/category/([^/]*)/page/([0-9]{1,})/?$', $this->blog_url),
			'index.php?category_name=$matches[1]&paged=$matches[2]', 
			'top'
		);

		add_rewrite_rule(
			sprintf('^%s/category/([^/]*)/?', $this->blog_url),
			'index.php?category_name=$matches[1]', 
			'top'
		);

	}

	public function modify_year_link( $yearlink, $year ) {

		$yearlink = str_replace( 
			sprintf( '/%s/', $year ), 
			sprintf( '/%s/%s/', $this->blog_url, $year ), 
			$yearlink 
		);

		return $yearlink;
	}


	public function redirect_year_url() {

		$year_link = get_year_link( get_query_var( 'year' ) );
		wp_redirect( $year_link , 301 );
		die();

	}


	// CATEGORY ==========================
	public function rewrite_category_url() {

		add_rewrite_rule(
			sprintf('^%s/category/([^/]*)/page/([0-9]{1,})/?$', $this->blog_url),
			'index.php?category_name=$matches[1]&paged=$matches[2]', 
			'top'
		);

		add_rewrite_rule(
			sprintf('^%s/category/([^/]*)/?', $this->blog_url),
			'index.php?category_name=$matches[1]', 
			'top'
		);

	}

	public function modify_category_link( $termlink ) {
		
		$termlink = str_replace( 
			'/category/', 
			sprintf( '/%s/category/', $this->blog_url ), 
			$termlink 
		);

		return $termlink;
	}


	public function redirect_category_url() {

		$cat_link = get_term_link( get_queried_object() );
		wp_redirect( $cat_link , 301 );
		die();

	}


	// Tag ==========================
	public function rewrite_tag_url() {

		add_rewrite_rule(
			sprintf('^%s/tag/([^/]*)/page/([0-9]{1,})/?$', $this->blog_url),
			'index.php?tag=$matches[1]&paged=$matches[2]', 
			'top'
		);

		add_rewrite_rule(
			sprintf('^%s/tag/([^/]*)/?', $this->blog_url),
			'index.php?tag=$matches[1]', 
			'top'
		);

	}

	public function modify_tag_link( $termlink ) {
		
		$termlink = str_replace( 
			'/tag/', 
			sprintf( '/%s/tag/', $this->blog_url ), 
			$termlink 
		);

		return $termlink;
	}


	public function redirect_tag_url() {

		$tag_link = get_term_link( get_queried_object() );
		wp_redirect( $tag_link , 301 );
		die();

	}


	// Author ==========================
	public function rewrite_author_url() {

		add_rewrite_rule(
			sprintf('^%s/author/([^/]*)/page/([0-9]{1,})/?$', $this->blog_url),
			'index.php?author_name=$matches[1]&paged=$matches[2]', 
			'top'
		);

		add_rewrite_rule(
			sprintf('^%s/author/([^/]*)/?', $this->blog_url),
			'index.php?author_name=$matches[1]', 
			'top'
		);

	}

	public function modify_author_link( $authorlink ) {
		
		$authorlink = str_replace( 
			'/author/', 
			sprintf( '/%s/author/', $this->blog_url ), 
			$authorlink 
		);

		return $authorlink;
	}


	public function redirect_author_url() {

		$author_link = get_author_posts_url( get_query_var( 'author' ) );
		wp_redirect( $author_link , 301 );
		die();

	}

	// Archive =========================
	public function rewrite_archive_url() {

		add_rewrite_rule(
			sprintf('^%s/page/([0-9]{1,})/?$', $this->blog_url),
			sprintf('index.php?post_type=post&paged=$matches[1]', $this->blog_url),
			'top'
		);

	}

	// Single ==========================
	public function rewrite_single_url() {

		add_rewrite_rule(
			sprintf('^%s/([^/]*)/?$', $this->blog_url),
			'index.php?name=$matches[1]', 
			'top'
		);

	}

	public function modify_single_link( $permalink, $post ) {
		
		$post_is_ready = in_array( $post->post_status, array('publish', 'private') );

		if( $post->post_type == 'post' && $post_is_ready ) {
			$permalink = home_url( $this->blog_url.'/'.$post->post_name );
		}

		return $permalink;
	}


	public function redirect_single_url() {

	    wp_redirect( get_permalink() , 301 );
	    exit();

	}

}

global $Blog_SEO_Rewrite;
$blog_seo_rewrite = new Blog_SEO_Rewrite;