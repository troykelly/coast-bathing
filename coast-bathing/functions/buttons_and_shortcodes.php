<?php

//BUTTON SHORTCODE
//ADD BUTTON SHORTCODE TO TINYMCE EDITOR

function button( $atts, $content = 'Click Here' ) {  
	extract(shortcode_atts(array(  
		"url" => '#'  
	), $atts));
	return '<a href="'. $url . '" class="button">'.$content.'</a>';  
}  
add_shortcode("button", "button"); 

add_action('init', 'add_button');

function add_button() {  
	if ( current_user_can('edit_posts') &&  current_user_can('edit_pages') && get_user_option('rich_editing'))  
	{  
		add_filter('mce_external_plugins', 'add_plugin');  
		add_filter('mce_buttons', 'register_button');  
	}  
}

function register_button($buttons) {  
	array_push($buttons, "button");   
	return $buttons;  
}

function add_plugin($plugin_array) {  
	$plugin_array['button'] = get_bloginfo('template_url').'/scripts/customcodes.js';  
	return $plugin_array;  
} 


// ----- custom wysiwyg formats
// function forge_custom_editor_style_button($buttons) {
// 	array_unshift($buttons, 'styleselect');
// 	return $buttons;
// }

// add_filter('mce_buttons_2', 'forge_custom_editor_style_button');


// function forge_custom_editor_styles( $init_array ) {
// // Define the style_formats array
// 	$style_formats = array(  
// 		// Each array child is a format with it's own settings
// 		array(
// 			'title' => 'Intro Text',
// 			'inline' => 'span',
// 			'classes' => 'intro-text',
// 			'wrapper' => true,
// 		)
// 	);  
// 	// Insert the array, JSON ENCODED, into 'style_formats'
// 	$init_array['style_formats'] = json_encode( $style_formats );  
// 	return $init_array;  
// } 
// // Attach callback to 'tiny_mce_before_init' 
// add_filter( 'tiny_mce_before_init', 'forge_custom_editor_styles' ); 


/*
 * Load typekit fonts into wp editor
============================================================================== */
// add_filter('mce_external_plugins', 'forge_tinymce_typekit_plugin' );
function forge_tinymce_typekit_plugin($plugins) {
    $plugins['fs_typekit'] = get_stylesheet_directory_uri().'/scripts/tinymce.fs_typekit.js';
    return $plugins;
}

// add_filter('tiny_mce_before_init', 'forge_tinymce_typekit_config');
function forge_tinymce_typekit_config($mceInit) {
	$mceInit['fs_typekit_id'] = 'xxxxxxx'; // change this to your typekit ID
	return $mceInit;
}
