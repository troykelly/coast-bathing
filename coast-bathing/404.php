<?php
/*
 * This is the template that displays on all pages by default.
 */
get_header(); ?>

	<div class="row content-area">

		<div id="content" class="columns-12 site-content" role="main">

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<header class="entry-header">
					
					<h1 class="entry-title"><?php the_field('404_page_title', 'options'); ?></h1>
				
				</header><!-- .entry-header -->
				
				<?php the_field('404_page_content', 'options'); ?>

				<?php if( get_field('404_display_search_form', 'options') ): ?>
						
					<form method="get" id="searchform" class="searchform s404" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">

						<input type="text" class="field" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" id="s" placeholder="<?php echo esc_attr_x( 'What are you looking for?', 'placeholder', 'forge_saas' ); ?>" />

						<input type="submit" class="submit" id="searchsubmit" value="<?php _e('Go', 'anvil')?>" />

					</form>

				<?php endif; ?>

				<?php if( get_field('404_display_back_to_home_button', 'options') ): ?>

					<a href="<?php bloginfo('url'); ?>" class="button"><?php the_field('404_back_to_home_button_text', 'options'); ?></a>

				<?php endif; ?>

			</article>

		</div>

	</div>
		
<?php get_footer(); ?>