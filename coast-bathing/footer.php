	</section><!-- #main -->
	<?php if(!is_page('contact')): ?>
		<?php get_template_part( 'templates/footer', 'callout' ); ?>
	<?php endif; ?>

	<footer class="site-footer" role="contentinfo">
		<div class="row">
			<div class="columns-12">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home" class="footer-logo">
					<img src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="site logo" />
				</a>
			</div>
			<div class="columns-11 right-1">
				<div class="footer-tagline">
					<h4><?php the_field('footer_tagline','options'); ?></h4>
				</div>
			</div>
		</div>
		
		<div class="row">

			<div class="columns-3 right-1">
				
				<div class="footer-location">
					<p><?php the_field('location','options'); ?></p>
					<p><?php the_field('email','options'); ?></p>
				</div>
			</div>
			<div class="columns-3 right-1">
				<?php wp_nav_menu( array( 'theme_location' => 'footer', 'items_wrap' => '<ul id="footer-nav" class="nav menu">%3$s</ul>' ) ); ?>
			</div>
			<div class="columns-3 right-1">
				<?php wp_nav_menu( array( 'theme_location' => 'footer2', 'items_wrap' => '<ul id="footer-nav2" class="nav menu">%3$s</ul>' ) ); ?>
			</div>
			<div class="columns-2 right-1">
				<div class="follow-us"><p>Follow Us</p></div>
				<ul class="footer-social-menu">
					<?php while(has_sub_field('social_media', 'options')): ?>
						<li><a class="ss-icon ss-social-regular" target="_blank" href="<?php the_sub_field('link', 'options'); ?>"><?php the_sub_field('social_media','options'); ?></a></li>
					<?php endwhile;?>
				</ul>
			</div>
		</div>

	

	</footer><!-- #colophon -->
	<div class="footer-end">
		<div class="row">
			<div class="columns-6">
			<p>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. All rights reserved.</p>
			<?php wp_nav_menu( array( 'theme_location' => 'footer3', 'items_wrap' => '<ul id="footer-nav3" class="nav menu">%3$s</ul>' ) ); ?>
		</div><!-- .site-info -->
		<div class="columns-6">
			<p>Crafted with love by <a href="http://forgeandsmith.com/" target="_blank">Forge and Smith</a>.</p>	
		</div>
		
	</div>
<?php wp_footer(); ?>
</div>

</body>

</html>