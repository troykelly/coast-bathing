(function() {  
    tinymce.create('tinymce.plugins.button', {  
        init : function(ed, url) {  
            ed.addButton('button', {  
                title : 'Add a Button',  
                image : url+'/button.png',  
                onclick : function() {  
                    var buttonLink = prompt("Button Link Address", "");
                    var buttonText = prompt("Button Text", "This is the button text");
                    ed.selection.setContent('[button url="' + buttonLink + '"]' + buttonText + '[/button]');  
                }  
            });  
        },  
        createControl : function(n, cm) {  
            return null;  
        },  
    });  
    tinymce.PluginManager.add('button', tinymce.plugins.button);  
})(); 