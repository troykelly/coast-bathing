jQuery(document).ready(function($) {

	$(".fs-mobile-trigger, .fs-mobile-trigger + .menu-slide").click(function(e){
		e.preventDefault();

		$('.fs-mobile-trigger').toggleClass('open');

		// slide down mobile menu
		// $('.mobile-nav-container').stop(true, true).slideToggle(200);
		// end slide down mobile menu

		// transition from right mobile menu 
		$('.mobile-nav-container').toggleClass('open');
		// end transition from right mobile menu 

	});

	$("#mobile-nav > li").find('.sub-menu').parent().children('a').after('<span class="menu-slide"></span>');

	$("#mobile-nav > li .menu-slide").on('click',function(e){
		$(this).toggleClass('open');
		e.preventDefault();
		e.stopPropagation();

		$(this).siblings('.sub-menu').stop(true, true).slideToggle(200);
	});

	// $('.slider').flexslider({
	// 	animation: "slide"
	// });
	$("select").wrap('<div class="styled-select"></div>');

});
jQuery(window).load(function() {


	$('.product-slider').flexslider({
		controlNav: false,
		animationLoop: false,
		directionNav: false,
		slideshow: false,
		init: function() {
			$(".gallery-control").css('opacity',1);
			$(".gallery-control li").click( function(e) {
				var index = $(this).index();
				$(".product-slider").flexslider(index);
			});
		}
	});

	

});